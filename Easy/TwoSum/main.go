package main

import "fmt"


func twoSum(nums []int, target int) []int {
	// Leetcode Part Starts Here
	m := make(map[int]int)

	for i, num := range(nums) {
		if _, ok := m[target - num]; ok {
			return []int{m[target - num], i}
	} else {
		m[num] = i
	}
	}
	return nil
	// Leetcode Part Ends Here
}

func main() {
	example1 := twoSum([]int{2,7,11,15}, 9)
	example2 := twoSum([]int{3,2,4}, 6)
	example3 := twoSum([]int{3,3}, 6)

	fmt.Println(example1)
	fmt.Println(example2)
	fmt.Println(example3)
}
