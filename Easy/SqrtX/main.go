package main

import (
	"fmt"
	"math"
)

func mySqrt(x int) int {
	x1 := float64(x)
	
	if x == 0 {
			return 0
	} 
	
	k := 1.0
	for ok := true; ok;
			ok = math.Abs(k*k-x1) >= 1 {
					k = (k+x1/k)/2
	}
	
	return int(k)
}

func main() {
	example1 := mySqrt(4)
	example2 := mySqrt(8)

	fmt.Println(example1)
	fmt.Println(example2)
}